FROM node:18

WORKDIR /usr/src/app

RUN npm install -g @angular/cli

COPY package.json package-lock.json ./

RUN npm install

EXPOSE 4200

CMD ng serve
