import { Component, OnInit } from '@angular/core';
import { Status } from '../interface/status';
import { StatusService } from '../utils/services/status.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  notification : Status = {
    response : '',
    type : 'info',
  
  }
  constructor(private statusService : StatusService) {}


  ngOnInit(): void {
    this.statusService.papmnotif$.subscribe(s => {
      this.notification = s;
    })
  }

  closeAlert() : void{
    this.statusService.notifPaPm({
      response : '',
      type : 'info',
    });
  }
}
