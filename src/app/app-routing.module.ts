import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CartComponent} from "./cart/components/cart/cart.component";
import {ProductComponent} from "./product/product.component";
import {RegisterComponent} from "./user/register/register.component";
import {LoginComponent} from "./user/login/login.component";
import {ProfilComponent} from "./user/profil/profil.component";
import { SearchComponent } from './main/component/search/search.component';
import { MainComponent } from './main/component/main/main.component';
import {IsConnectedGuard} from "./guards/is-connected.guard";
import {IsNotConnectedGuard} from "./guards/is-not-connected.guard";

const routes: Routes = [
    {path: "game/:save", loadChildren: () => import("./game/game.module").then(m => m.GameModule), canActivate: [IsConnectedGuard], canLoad: [IsConnectedGuard]},
    {path: "game", loadChildren: () => import("./game/game.module").then(m => m.GameModule), canActivate: [IsConnectedGuard], canLoad: [IsConnectedGuard]},
    {path: "buy", component: CartComponent},
    {path: "product/:type/:id", component: ProductComponent},
    {path: "register", component: RegisterComponent, canActivate: [IsNotConnectedGuard], canLoad: [IsNotConnectedGuard]},
    {path: "login", component: LoginComponent, canActivate: [IsNotConnectedGuard], canLoad: [IsNotConnectedGuard]},
    {path: "profil", component: ProfilComponent, canActivate: [IsConnectedGuard], canLoad: [IsConnectedGuard]},
    {path: "", component: MainComponent},
    {path: "search", component: SearchComponent},
    {path: "search/:type", component: SearchComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
