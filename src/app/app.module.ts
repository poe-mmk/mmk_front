import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './main/component/carousel/carousel.component';
import { CardComponent } from './card/card.component';
import { MainComponent } from './main/component/main/main.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { ConnectionComponent } from './utils/components/connection/connection.component';
import {ReactiveFormsModule} from "@angular/forms";
import {RedirectInterceptor} from "./utils/interceptors/redirect/redirect.interceptor";
import { CartComponent } from './cart/components/cart/cart.component';
import { ProductRowComponent } from './cart/components/product-row/product-row.component';
import { ProductComponent } from './product/product.component';
import {RegisterComponent} from "./user/register/register.component";
import { LoginComponent } from './user/login/login.component';
import { ProfilComponent } from './user/profil/profil.component';
import { SearchComponent } from './main/component/search/search.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { AlertComponent } from './alert/alert.component';
import { ArticleModalComponent } from './article-modal/article-modal.component';
import { LootModalComponent } from './loot-modal/loot-modal.component';

@NgModule({
    declarations: [
            AppComponent,
            ConnectionComponent,
            CartComponent,
            ProductRowComponent,
            ProductComponent,
            RegisterComponent,
            LoginComponent,
            ProfilComponent,
            NavbarComponent,
            FooterComponent,
            CarouselComponent,
            CardComponent,
            MainComponent,
            ConnectionComponent,
            SearchComponent,
            AlertComponent,
            ArticleModalComponent,
            LootModalComponent
        ],
    imports: [
            BrowserModule,
            AppRoutingModule,
            HttpClientModule,
            ReactiveFormsModule,
            NgxSliderModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: RedirectInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
