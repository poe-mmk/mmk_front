import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { StatusModal } from '../interface/statusModal';
import { StatusService } from '../utils/services/status.service';

@Component({
  selector: 'app-article-modal',
  templateUrl: './article-modal.component.html',
  styleUrls: ['./article-modal.component.css']
})
export class ArticleModalComponent implements OnInit {

  sub : Subscription = new Subscription();

  notification : StatusModal = {
    response : '',
    type : null,
    value : "",
    name : ""
  }
  constructor(private statusService : StatusService) { }

  ngOnInit(): void {
    this.sub = this.statusService.notificationEnvoye$.subscribe((status) => {
      this.notification = status;
      if(status.value != null){
          if(status.value === "item"){

          }else {
            this.notification.name= status.name;
            this.notification.value = status.value;
          }
      }
    })
  }

  ngOnDestroy():void{
    this.sub.unsubscribe();
  }
  closeAlert() : void{
    this.statusService.envoyerStatus({
      response : '',
      type : null,
      value : null,
      name: null
    });
  }
}
