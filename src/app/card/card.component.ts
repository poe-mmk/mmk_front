import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from '../product/product.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() big: boolean = false;
  format: string[] =
  ["col-12 col-sm-6 col-lg-4 col-xl-3 my-2", // <!--Card medium taille-->
  "d-flex justify-content-center col-12 col-md-4 mb-1 p-0" ] //<!--Card big taille-->

  @Input() png: string =  "url('https://via.placeholder.com/400')";
  @Input() name: string ="bob";
  @Input() price: number | undefined = 300;
  @Input() type: "personage" | "armor" | "weapon" = "personage";
  @Input() id: number=1;


  constructor(private productService: ProductService) {  }

  ngOnInit(): void {
    
  }

  addArticle(){
    this.productService.addToCard(this.id, this.type, this.name, this.png);
  }
  addWishList(){
    
  }

}
