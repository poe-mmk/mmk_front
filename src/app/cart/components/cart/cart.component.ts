import { Component, OnInit } from '@angular/core';
import {Cart} from "../../../interface/cart";
import {CartService} from "../../services/cart.service";
import {StatusService} from "../../../utils/services/status.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart: Cart = {
    staticArmor: null,
    staticWeapon: null,
    hero: null,
    inventory: [],
    stages: [],
    savedHero: null
  };

  constructor(private cartService: CartService, private statusService: StatusService) { }

  ngOnInit(): void {
    this.cartService.getCart().subscribe(r => {
      if (r != null)
        this.cart = r;
    });
  }

  removeItem(event: number) {
    switch (event) {
      case 0:
        this.cart.hero = null;
        this.cart.savedHero = null;
        break;
      case 1:
        this.cart.staticArmor = null;
        break;
      case 2:
        this.cart.staticWeapon = null;
        break;
    }
  }

  buy(): void {
    if (this.cart.savedHero === null && (this.cart.stages === [] || this.cart.hero === null || this.cart.staticWeapon === null || this.cart.staticArmor === null))
      this.statusService.notifPaPm({type: 'info', response: 'votre panier n\'est pas complet'});
    else
      this.cartService.buy().subscribe(r => console.log(r));
  }

  getTotalPrice(): number {
    let total = 0;
    total += (this.cart.hero != null)?this.cart.hero.price!:0;
    total += (this.cart.staticArmor != null)?this.cart.staticArmor.price!:0;
    total += (this.cart.staticWeapon != null)?this.cart.staticWeapon.price!:0;
    total = (this.cart.savedHero != null)?0:total;

    return total;
  }

  addStage(event: Event) {
    let e = event.target as HTMLInputElement;
    if (e) {
      this.cartService.addStage(e.value).subscribe(_ => this.cart.stages.push(parseInt(e.value)))
    }
  }

  removeStage(i: number) {
    this.cartService.removeStage(i).subscribe(_ => this.cart.stages.splice(i, 1));
  }

}
