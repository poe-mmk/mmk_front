import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Armor} from "../../../interface/armor";
import {Weapon} from "../../../interface/weapon";
import {Inventory} from "../../../interface/inventory";
import {CartService} from "../../services/cart.service";
import {StaticPersonage} from "../../../interface/staticPersonage";

@Component({
  selector: 'app-product-row',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.css']
})
export class ProductRowComponent implements OnInit {

  @Output() removeItemEvent = new EventEmitter<number>();

  @Input() product : Armor | Weapon | StaticPersonage | Inventory = {} as Armor;
  quantity: number = 1;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }

  changeQuantity(n: number): void {
    this.quantity += n;
    this.quantity = (this.quantity<1)?1:this.quantity;
  }

  removeItem() {
    if ("id" in this.product)
      this.cartService.removeItem(this.product.id, this.getType(this.product)).subscribe();
    else if ("idItem" in this.product)
      this.cartService.removeItem(this.product.idItem, this.getType(this.product)).subscribe();
    this.removeItemEvent.emit(this.getType(this.product));
  }

  getType(product: Armor | Weapon | StaticPersonage | Inventory): number {
    if (this.isPersonage(product))
      return 0;
    if (this.isArmor(product))
      return 1;
    if (this.isWeapon(product))
      return 2;
    if (product.type === "consumable")
      return 3;
    if (product.type === "card")
      return 4;
    return -1;
  }

  isArmor(product: any): product is Armor {
    return "defence" in product;
  }

  isWeapon(product: any): product is Weapon {
    return "damage" in product;
  }

  isPersonage(product: any): product is StaticPersonage {
    return "hp" in product;
  }

}
