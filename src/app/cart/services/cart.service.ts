import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cart} from "../../interface/cart";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpClient: HttpClient) { }

  getCart(): Observable<Cart> {
    return this.httpClient.get<Cart>("http://localhost:8080/api/cart/get");
  }

  buy(): Observable<any> {
    return this.httpClient.post("http://localhost:8080/api/cart/buy", {});
  }

  addStage(difficulty: string): Observable<any> {
    return this.httpClient.put("http://localhost:8080/api/cart/addstage/"+difficulty, {});
  }

  removeStage(i: number): Observable<any> {
    return this.httpClient.delete("http://localhost:8080/api/cart/removestage/"+i);
  }

  removeItem(articleId: number, articleType: number): Observable<any> {
    return this.httpClient.delete("http://localhost:8080/api/cart/removearticle/"+articleType+"/"+articleId)
  }
}
