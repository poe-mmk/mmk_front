import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActionService} from "../../services/action/action.service";
import {StatusService} from "../../../utils/services/status.service";
import {catchError, EMPTY} from "rxjs";

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {

  @Output() newRound = new EventEmitter<string>();
  @Output() newBoard = new EventEmitter<string>();
  @Output() newAction = new EventEmitter<{type:string, arg:number}>();

  constructor(private actionService: ActionService, private statusService: StatusService) { }

  ngOnInit(): void {
    this.actionService.newBoard$.subscribe(_ => this.newBoard.emit(""));
    this.actionService.newRound$.subscribe(_ => this.newRound.emit(""));
  }

  attackClick(): void {
    this.actionService.setAttackAction()
        .pipe(
            catchError(r => {
              if (r.status === 403)
                this.statusService.notifPaPm({type: 'info', response: "plus de point d'action"});
              return EMPTY;
            })
        )
        .subscribe(_ => {
            this.newAction.emit({type:"attack", arg:0});
        })
  }

  moveClick(direction: number): void {
    this.actionService.setMoveAction(direction)
        .pipe(
            catchError(r => {
              if (r.status === 403)
                this.statusService.notifPaPm({type: 'info', response: "plus de point de mouvement"});
              return EMPTY
            })
        )
        .subscribe(_ => {
            this.newAction.emit({type:"move", arg:direction});
        })
  }

  blockClick(): void {
    this.actionService.setBlockAction()
        .pipe(
            catchError(r => {
              if (r.status === 403)
                this.statusService.notifPaPm({type: 'info', response: "plus de point d'action"});
              return EMPTY;
            })
        )
        .subscribe(_ => {
            this.newAction.emit({type:"block", arg:0});
        })
  }

  jumpClick(): void {
    this.actionService.setJumpAction()
        .pipe(
            catchError(r => {
              if (r.status === 403)
                this.statusService.notifPaPm({type: 'info', response: "plus de point de mouvement"});
              return EMPTY;
            })
        )
        .subscribe(_ => {
            this.newAction.emit({type:"jump", arg:0});
        })
  }

  playClick(): void {
    this.actionService.play();
  }

}
