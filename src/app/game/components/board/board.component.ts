import { Component, OnInit } from '@angular/core';
import {Board} from "../../../interface/board";
import {Personage} from "../../../interface/personage";
import {BoardService} from "../../services/board/board.service";
import {Background} from "../../../interface/background";
import {Inventory} from "../../../interface/inventory";
import {ActivatedRoute} from "@angular/router";
import {ActionDto} from "../../../interface/action-dto";
import {ActionService} from "../../services/action/action.service";
import { Armor } from 'src/app/interface/armor';
import { Weapon } from 'src/app/interface/weapon';
import { StatusService } from 'src/app/utils/services/status.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  board: Board = {background: {} as Background, personages: [], id: 0, stage: 0, powerLevel: 0};
  start: number = 0;
  heroInventory: Inventory[] = [];
  actions: ActionDto[] =[];
    constructor(private roote: ActivatedRoute, private boardService: BoardService, private actionService: ActionService, private statusService: StatusService) {
    }

    ngOnInit(): void {
        if (this.roote.snapshot.params["save"])
            this.boardService.selectSave(this.roote.snapshot.params["save"]).subscribe(_ => {
                this.getBackground();
                this.getPersonage();
                this.getHeroInventory();
            })
        else {
            this.getBackground();
            this.getPersonage();
            this.getHeroInventory();
        }

        this.actionService.actionHistory$.subscribe(h => {
            this.actions= h;
            this.animationMob();
        });
    }

    addIdleAnimation() {
        document.querySelectorAll(".personage").forEach( (e) => {
            let i = e as HTMLImageElement;
            for (let p of this.board.personages)
                if (p.id == parseInt(i.id))
                    this.setImage(i, p, "idle");
        })
    }

    getHeroInventory() {
        this.boardService.getHeroInventory().subscribe(r => {
            this.heroInventory = r;
        })
    }

    getBackground() {
        this.boardService.getBackground().subscribe(b => {
            this.board.background = b;
        });
    }

    getPersonage() {
        this.boardService.getPersonages().subscribe(p => {
            this.board.personages = p;
            this.getHeroInventory();
            this.windowPositionCompute();
        });
    }

    isPersonage(x: number, y: number): boolean {
        for (let p of this.board.personages)
            if (p.stats.position.x === x && p.stats.position.y === y)
                return true
        return false;
    }

    isHero(x: number, y: number): boolean {
        for (let p of this.board.personages)
            if (p.stats.position.x === x && p.stats.position.y === y && p.iaId == null)
                return true
        return false;
    }

    getPersonageFromBoard(x: number, y: number): Personage {
        for (let p of this.board.personages)
            if (p.stats.position.x === x && p.stats.position.y === y)
                return p;
        return {} as Personage;
    }

    getHero(): Personage | null {
        for (let personage of this.board.personages)
            if (personage.iaId === null)
                return personage;
        return null;
    }

    getPersonageById(id: number): Personage | null {
        for (let p of this.board.personages)
            if (p.id == id)
                return p;
        return null;
    }

    windowPositionCompute() : void {
      if (this.board === undefined)
          return ;

      let hero: Personage | null = this.getHero();

      if (hero === null)
          return ;

      if (hero.stats.position.x < 4)
          this.start = 0;
      else
          this.start = hero.stats.position.x-3;
      if (this.start + 10 > this.board.background.width)
          this.start = this.board.background.width - 10;

    setTimeout(() => this.addIdleAnimation(), 5);
    }

    animAction(event: {type: string, arg: number}, personage: Personage) {
        const personageHtml = document.getElementById(String(personage.id)) as HTMLImageElement;
        switch (event.type) {
            case "attack":
            case "ATTACK":
                this.setAnimation(personageHtml, personage, "attack", 1000);
                let p = this.getInRange(personage);
                if (p)
                    this.setAnimation(document.getElementById(String(p.id))! as HTMLImageElement, p, "hurt", 1000);
                break;
            case "move":
            case "MOVE":
                if (event.arg == personage.stats.direction.x) {
                    let move = true;
                    for (let p of this.board.personages)
                        if (p.stats.position.x == personage.stats.position.x + event.arg)
                            move = false;
                    if (move) {
                        this.setAnimation(personageHtml, personage, "move", 1000);
                        setTimeout(() => {
                            personage.stats.position.x += personage.stats.direction.x
                            this.windowPositionCompute();
                        }, 1000);
                    }
                } else {
                    personage.stats.direction.x = event.arg;
                    this.addIdleAnimation();
                }
                break;
            case "jump" :
            case "JUMP" :
                let jump = true;
                for (let p of this.board.personages)
                    if (p.stats.position.x == personage.stats.position.x + 2*personage.stats.direction.x)
                        jump = false;
                if (jump) {
                    this.setAnimation(personageHtml, personage, "move", 1000);
                    setTimeout(() => {
                        personage.stats.position.x += 2*personage.stats.direction.x
                        this.windowPositionCompute();
                    }, 1000);
                }
        }
    }

    setAnimation(element: HTMLImageElement, personage: Personage, animation: string, time: number) {
        this.setImage(element, personage, animation);

        setTimeout(() => {
            this.setImage(element, personage, "idle");
        }, time+3)
    }

    setImage(element: HTMLImageElement, personage: Personage, animation: string) {
        let step = this.getAnimationFrameCount(personage, animation);
        element.src = "../../../../assets/images/personage/"+personage.iconURL+"/"+animation+".png";
        element.width = 128 * step;
        element.style.animationTimingFunction = "steps("+step+", end)";
        if (animation == "idle") {
            element.style.animationIterationCount = "infinite";
            element.style.animationDuration = "1.5s";
        }
        else {
            element.style.animationIterationCount = "1";
            element.style.animationDuration = "1s";
        }
        element.style.animationPlayState = "running"
        switch (animation) {
            case "idle":
                element.style.animationName = ((personage.stats.direction.x==1)?"idleRight":"idleLeft");
                break;
            case "attack":
                element.style.animationName = "attackSpritesheet"+((personage.stats.direction.x==1)?0:1);
                break;
            case "move":
                element.style.animationName = "moveSpritesheet"+((personage.stats.direction.x==1)?0:1);
                break;
            case "hurt":
                element.style.animationName = "hurtSpritesheet"+((personage.stats.direction.x==1)?0:1);
                break;
        }
    }

    getInRange(personage: Personage): Personage|null {
        for (let p of this.board.personages)
            if (personage.stats.position.x+(personage.weapon.minRange*personage.stats.direction.x) <= p.stats.position.x
                && p.stats.position.x <= personage.stats.position.x+(personage.weapon.maxRange*personage.stats.direction.x))
                return p;
        return null;
    }

    animationMob() {
        if (this.actions.length == 0) {
            this.getPersonage();
            return;
        }
        if (this.actions[0].action === "LOOT") {
            console.log("Loot Detecté")
            let loot = this.actions[0].args;
            for (let i = 0; i < loot.length; i++) {
              if (loot[i] != null) {
                console.log(loot[i]);
                if (this.isArmor(loot[i])) {
                  let armor = loot[i] as Armor;
                  console.log("Cast armor " + armor.description);
                  //shift loot[i]
                  this.statusService.envoyerStatusLoot({
                    value: armor
                  });

                } else if (this.isWeapon(loot[i])) {
                  let weapon = loot[i] as Weapon;
                  console.log("Cast weapon " + weapon.description);
                  this.statusService.envoyerStatusLoot({
                    value: weapon
                  });
                }
              }
            }
          }
        if (this.actions.length == 0) {
            this.getPersonage();
            return;
        }

        if (this.actions[0].idPersonage == this.getHero()!.id) {
            this.actions.shift();
            this.animationMob();
            return ;
        }

        let doc = document.getElementById(String(this.actions[0].idPersonage));
        let p = this.getPersonageById(this.actions[0].idPersonage);

        if (doc != null && p != null) {
            this.animAction({type: this.actions[0].action, arg: this.actions[0].arg}, p);
            this.actions.shift();
            setTimeout(() => this.animationMob(), 1005);
        } else {
            this.actions.shift();
            this.animationMob();
        }
    }

    getAnimationFrameCount(p: Personage, a: string): number {
        switch (a) {
            case "attack":
                return parseInt(p.animationFrame.split(',')[0]);
            case "hurt" :
                return parseInt(p.animationFrame.split(',')[2]);
            case "idle" :
                return parseInt(p.animationFrame.split(',')[3]);
            case "move":
            case "jump" :
                return parseInt(p.animationFrame.split(',')[4]);
            default:
                return 0;
        }
    }

    isArmor(o: any): o is Armor{
        return "defence" in o;
    }
    isWeapon(o:any): o is Weapon{
        return "damage" in o;
    }
}
