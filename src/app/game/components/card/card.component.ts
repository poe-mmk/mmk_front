import {Component, Input, OnInit} from '@angular/core';
import {Inventory} from "../../../interface/inventory";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() heroInventory: Inventory[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
