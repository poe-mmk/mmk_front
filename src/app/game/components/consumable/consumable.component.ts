import {Component, Input, OnInit} from '@angular/core';
import {ActionService} from "../../services/action/action.service";
import {Inventory} from "../../../interface/inventory";
import {BoardService} from "../../services/board/board.service";

@Component({
  selector: 'app-consumable',
  templateUrl: './consumable.component.html',
  styleUrls: ['./consumable.component.css']
})
export class ConsumableComponent implements OnInit {

  @Input() heroInventory: Inventory[] = [];

  constructor(private actionService: ActionService) { }

  ngOnInit(): void {

  }

  consume(id: number): void {
    this.actionService.setConsumeAction(id).subscribe(r => {
      if (r.status === 403)
        alert(r.message);
      else
        for (let i of this.heroInventory)
          if (i.idItem === id)
            i.amount--;
    })
  }


}
