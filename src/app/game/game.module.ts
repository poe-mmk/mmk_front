import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BoardComponent} from "./components/board/board.component";
import {HttpClientModule} from "@angular/common/http";
import { GameRootingModule } from './game-rooting/game-rooting.module';
import {ReactiveFormsModule} from "@angular/forms";
import { ActionComponent } from './components/action/action.component';
import { ConsumableComponent } from './components/consumable/consumable.component';
import { CardComponent } from './components/card/card.component';



@NgModule({
  declarations: [
      BoardComponent,
      ActionComponent,
      ConsumableComponent,
      CardComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    GameRootingModule,
    ReactiveFormsModule,
  ]
})
export class GameModule { }
