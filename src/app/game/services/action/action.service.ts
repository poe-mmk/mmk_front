import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {ResponseMessage} from "../../../interface/response-message";
import {Personage} from "../../../interface/personage";
import {ActionDto} from "../../../interface/action-dto";

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  private actionHistory = new BehaviorSubject<ActionDto[]>([]);
  private newRound = new BehaviorSubject<string>("");
  private newBoard = new BehaviorSubject<string>("");
  actionHistory$ = this.actionHistory.asObservable();
  newRound$ = this.newRound.asObservable();
  newBoard$ = this.newBoard.asObservable();

  constructor(private httpClient: HttpClient) { }

  setAttackAction(): Observable<ResponseMessage> {
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/action/set/attack", {});
  }

  setBlockAction(): Observable<ResponseMessage> {
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/action/set/block", {});
  }

  setConsumeAction(idConsumable: number): Observable<ResponseMessage> {
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/action/set/consume", {"idConsumable": idConsumable});
  }

  setMoveAction(direction: number): Observable<ResponseMessage> {
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/action/set/move", {"direction": direction});
  }

  setJumpAction(): Observable<ResponseMessage> {
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/action/set/jump", {});
  }

  setPlayAction(): Observable<ResponseMessage | ActionDto[]> {
    return this.httpClient.post<ResponseMessage | ActionDto[]>("http://localhost:8080/api/action/play", {});
  }

  play() {
    this.setPlayAction().subscribe(r => {
      if ("status" in r) {
        if (r.status === 201)
          this.newBoard.next("new Board");
      }
      else {
        this.newRound.next("new round")
        this.actionHistory.next(r);
      }
    })
  }

  resetAction(): Observable<Personage> {
    return this.httpClient.post<Personage>("http://localhost:8080/api/action/reset", {});
  }
}
