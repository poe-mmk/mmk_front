import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Personage} from "../../../interface/personage";
import {Background} from "../../../interface/background";
import {Inventory} from "../../../interface/inventory";

@Injectable({
  providedIn: 'root'
})
export class BoardService {

  constructor(private httpClient: HttpClient) { }

    selectSave(n: number): Observable<any> {
      return this.httpClient.post("http://localhost:8080/api/save/select", {id: n});
    }

    getBackground(): Observable<Background> {
      return new Observable<Background>(subscriber =>
          this.httpClient
              .get<Background>("http://localhost:8080/api/save/background/get")
              .subscribe(b => {
                  subscriber.next(this.boardBackgroundCompute(b));
              }));
    }

    getPersonages(): Observable<Personage[]> {
      return this.httpClient.get<Personage[]>("http://localhost:8080/api/save/personages/get");
    }

    getHeroInventory(): Observable<Inventory[]> {
      return this.httpClient.get<Inventory[]>("http://localhost:8080/api/save/personages/inventory/get");
    }

    boardBackgroundCompute(background: Background) {
        let newBackground: Background = {width: background.width, height: background.height, background: []};
        const width = background.width;
        const string = background.background as unknown as string;

        const stringTab = string.split("/");


        let j=0;
        for (const s of stringTab) {
            const stab = s.split(",");

            let i = 0
            for (; i < parseInt(stab[1]); i++)
                if (Array.isArray(newBackground.background[Math.floor((i + j) / width)]))
                    newBackground.background[Math.floor((i + j) / width)][(i + j) % width] = stab[0];
                else
                    newBackground.background[Math.floor((i + j) / width)] = [stab[0]];
            j += i;
        }
        return newBackground;
    }
}
