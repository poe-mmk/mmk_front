import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate, CanLoad, Route,
  Router,
  RouterStateSnapshot, UrlSegment,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthStatus} from "../interface/auth-status";
import {LoginService} from "../utils/services/login.service";

@Injectable({
  providedIn: 'root'
})
export class IsNotConnectedGuard implements CanActivate, CanLoad {
  authStatus: AuthStatus = {connected: false};

  constructor(private loginService: LoginService, private router: Router) {
    loginService.utilisateurLog$.subscribe(a => {
      this.authStatus = a;
    })
  }

  canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authStatus.connected)
      this.router.navigateByUrl("/profil");
    return !this.authStatus.connected;
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authStatus.connected)
      this.router.navigateByUrl("/profil");
    return !this.authStatus.connected;
  }
  
}
