export interface Armor {
  id: number,
  name: string,
  description: string,
  iconURL: string,
  defence: number,
  price?: number
}
