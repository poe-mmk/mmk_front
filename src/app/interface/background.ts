export interface Background {
  background: string[][],
  width: number,
  height: number
}
