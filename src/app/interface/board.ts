import {Personage} from "./personage";
import {Background} from "./background";

export interface Board {
  id: number,
  background: Background,
  powerLevel: number,
  stage: number,
  personages: Personage[]
}
