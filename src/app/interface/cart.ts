import {Armor} from "./armor";
import {Weapon} from "./weapon";
import {Inventory} from "./inventory";
import {StaticPersonage} from "./staticPersonage";

export interface Cart {
    staticArmor: Armor | null,
    staticWeapon: Weapon | null,
    hero: StaticPersonage | null,
    inventory: Inventory[],
    stages: number[],
    savedHero: StaticPersonage | null
}
