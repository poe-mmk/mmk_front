export interface Highligth {
    id: number,
    name: string,
    description: string,
    iconurl: string,
    price: number,
    type: "armor" | "personage" | "weapon"
}