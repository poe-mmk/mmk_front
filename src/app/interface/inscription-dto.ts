export interface InscriptionDto {
    firstName: string,
    lastName: string,
    pseudo: string,
    password: string,
    email: string
}
