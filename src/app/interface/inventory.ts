export interface Inventory {
  idItem: number,
  name: string,
  description: string,
  iconurl: string,
  amount: number,
  type: string,
  price?: number
}