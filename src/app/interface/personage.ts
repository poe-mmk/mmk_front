import {Stats} from "./stats";
import {Weapon} from "./weapon";
import {Armor} from "./armor";

export interface Personage {
  id: number,
  name: string,
  description: string,
  iconURL: string,
  iaId: number | null,
  stats: Stats,
  weapon: Weapon,
  armor: Armor,
  price?: number,
  animationFrame: string
}
