export interface ResponseMessage {
    status: number,
    message: string
}
