import {Board} from "./board";

export interface Save {
  id: number,
  boards: Board[],
  currentBoard: number
}
