export interface StaticItem {
    id: number;
    name: string;
    iconURL: string;
    description: string;
    type: "consumable";
}