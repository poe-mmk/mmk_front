export interface StaticPersonage {
    id: number;
    iconURL:string;
    name: string;
    description: string;
    price: number;
    hp: number;
    pa: number;
    pm: number;
    strength: number;
}