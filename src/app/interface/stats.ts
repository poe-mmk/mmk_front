import {Vector2} from "./vector2";

export interface Stats {
  id: number,
  blackgold: number,
  position: Vector2,
  direction: Vector2,
  hp: number,
  maxHp: number,
  pa: number,
  pm: number,
  personageStage: number,
  strength: number,
  currentPa: number,
  currentPm: number
}
