import { Armor } from "./armor";
import { StaticItem } from "./staticItems";
import { Weapon } from "./weapon";

export interface StatusLoot {
    value: Armor | Weapon | StaticItem[] | null;
}