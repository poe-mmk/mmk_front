import { Armor } from "./armor";
import { StaticPersonage } from "./staticPersonage";
import { Weapon } from "./weapon";

export interface StatusModal {
    response: string;
    type : "personage" | "armor" | "weapon" | null;
    value: string | null;
    name: string | null;
}
