import {Save} from "./save";

export interface User {
  id: number,
  lastName: string,
  firstName: string,
  pseudo: string,
  email: string,
  blackgold: number,
  saves: Save[]
}
