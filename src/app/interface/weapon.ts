export interface Weapon {
  id: number,
  name: string,
  description: string,
  iconURL: string,
  damage: number,
  maxRange: number,
  minRange: number,
  price?: number
}
