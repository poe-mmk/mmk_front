import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LootModalComponent } from './loot-modal.component';

describe('LootModalComponent', () => {
  let component: LootModalComponent;
  let fixture: ComponentFixture<LootModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LootModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LootModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
