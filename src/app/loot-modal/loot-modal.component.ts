import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Armor } from '../interface/armor';
import { StatusLoot } from '../interface/statusLoot';
import { Weapon } from '../interface/weapon';
import { StatusService } from '../utils/services/status.service';

import * as bootstrap from 'bootstrap';
import { LootService } from '../utils/services/loot.service';


@Component({
  selector: 'app-loot-modal',
  templateUrl: './loot-modal.component.html',
  styleUrls: ['./loot-modal.component.css']
})
export class LootModalComponent implements OnInit {

  sub: Subscription = new Subscription();

  notification: StatusLoot ={
    value:null
  }
  constructor(private statusService: StatusService, private lootService: LootService) { }

  ngOnInit(): void {
    this.sub = this.statusService.notificationEnvoyeLoot$.subscribe((status) => {
      this.notification = status;
      console.log(status.value!+"notif bien arrivé");
      //$("#modalLoot").modal("show");
  
      var myModal = new bootstrap.Modal(<Element>(document.getElementById("modalLoot")));
      myModal.show();
    });

  }

  ngOnDestroy():void{
    var myModal = new bootstrap.Modal(<Element>(document.getElementById("modalLoot")));
    myModal.hide();
    this.sub.unsubscribe();
  }
  closeAlert() : void{
    var myModal = new bootstrap.Modal(<Element>(document.getElementById("modalLoot")));
    myModal.dispose();
  }

  refused(){    
    this.lootService.refusedLoot().subscribe();
    this.closeAlert();
  }
  acceptedArmor(){
    this.lootService.acceptArmor().subscribe();
    this.closeAlert();
  }
  acceptedWeapon(){
    this.lootService.acceptWeapon().subscribe();
    this.closeAlert();
  }
  isArmor(o: any): o is Armor {

    return "defence" in o;
  }

  isWeapon(o: any): o is Weapon {

    return "damage" in o;
  }
}
