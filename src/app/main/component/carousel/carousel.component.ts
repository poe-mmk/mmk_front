import { Component, OnInit } from '@angular/core';
import { Highligth } from '../../../interface/highligth';
import { CarouselService } from '../../services/carousel.service';
import { StaticPersonage } from '../../../interface/staticPersonage';
import { Armor } from 'src/app/interface/armor';
import { Weapon } from 'src/app/interface/weapon';

@Component({
  selector: 'app-carousel',
  templateUrl: 'carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

    high: Highligth []= [];

  constructor(private carouselService: CarouselService) { }

  ngOnInit(): void {
    this.carouselService.getHighLigth().subscribe((items)=> {
        for(let i =0; i <items.length; i++){
            if(this.isArmor(items[i])){
                const a: Armor = items[i] as Armor;
                this.high[i]= {
                    id: a.id,
                    name: a.name,
                    description: a.description,
                    iconurl: a.iconURL,
                    price: a.price!,
                   type: "armor"
                };
            }else if(this.isWeapon(items[i])){
                const w: Weapon= items[i] as Weapon;
                this.high[i]= {
                    id: w.id,
                    name: w.name,
                    description: w.description,
                    iconurl: w.iconURL,
                    price: w.price!,
                   type: "weapon"
                };
            }else if(this.isPersonage(items[i])){
                const p: StaticPersonage = items[i] as StaticPersonage;
                this.high[i]={
                    id: p.id,
                    name: p.name,
                    description: p.description,
                    iconurl: p.iconURL,
                    price: p.price,
                   type: "personage"
                };
            }

        }
    });


    document.getElementById("bLeft")?.addEventListener("click", () => {
      this.changePosition((x: number) => (x+3)%5-2, true);
    });
  
  document.getElementById("bRight")?.addEventListener("click", () => {
      this.changePosition((x: number) => 
         (x-3)%5+2, false);
  });
  
  }

  isArmor(object: any): object is Armor{
    return  'defence' in object;
  }
  isWeapon(object: any): object is Weapon{
    return 'damage' in object;
  }
  isPersonage(object: any): object is StaticPersonage{
    return 'hp' in object;
  }

 changePosition = (f: { (x: number): number; (x: number): number; (arg0: number): any; }, left:boolean) => {
    const cards:HTMLCollectionOf<any>= document.getElementsByClassName("mycarousel");
    let anims = [];
    for (let i=0;i<cards.length;i++) {

        anims[parseInt(cards[i].dataset.myPosition)+2] = cards[i];

        cards[i].dataset.myPosition = f(parseInt(cards[i].dataset.myPosition));

        let y = setTimeout(() => {
            switch (cards[i].dataset.myPosition) {
                case "-2" :
                    cards[i].className = "col-2 align-self-end mycarousel order-1 ";
                    cards[i].style.zIndex = "1";

                    break;
                case "-1" :
                    cards[i].className = "col-2 align-self-center mycarousel order-2";
                    cards[i].style.zIndex = "3";
                    break;
                case "0" :
                    cards[i].className = "col-4 align-self-start mycarousel order-3";
                    cards[i].style.zIndex = "3";
                    break;
                case "1" :
                    cards[i].className = "col-2 align-self-center mycarousel order-4";
                    cards[i].style.zIndex = "3";
                    break;
                case "2" :
                    cards[i].className = "col-2 align-self-end mycarousel order-5";
                    cards[i].style.zIndex = "1";
                    break;
            }
        }, 550);

    }

    if (left) {
        this.animate(anims[0], anims[1]);
        this.animate(anims[1], anims[2]);
        this.animate(anims[2], anims[3]);
        this.animate(anims[3], anims[4]);
        this.animate(anims[4], anims[0]);
    }else {
        this.animate(anims[0], anims[4]);
        this.animate(anims[1], anims[0]);
        this.animate(anims[2], anims[1]);
        this.animate(anims[3], anims[2]);
        this.animate(anims[4], anims[3]);

    }
}
 diff = (from:Element, to:Element) => {
    const fromPos = from.getBoundingClientRect();
    const toPos = to.getBoundingClientRect();

    return [{
        position: "absolute",
        top: fromPos.top + "px",
        left: fromPos.left + "px",
        width: fromPos.width + "px",
        height: fromPos.height + "px"
    }, {
        position: "absolute",
        top: toPos.top + "px",
        left: toPos.left + "px",
        width: toPos.width + "px",
        height: toPos.height + "px"
    }];
}

animate = (from:Element, to:Element) => {
    const difff = this.diff(from, to);

    from.animate(difff, {
        // temporisation
        delay: 5,
        duration: 500,
        iterations: 1
    });
}

}
