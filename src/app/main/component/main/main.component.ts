import { Component, OnInit } from '@angular/core';
import { CardComponent } from '../../../card/card.component';
import { Armor } from '../../../interface/armor';
import { StaticPersonage } from '../../../interface/staticPersonage';
import { Weapon } from '../../../interface/weapon';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  format: string[] =
  ["col-12 col-sm-6 col-lg-4 col-xl-3 my-2", // <!--Card medium taille-->
  "d-flex justify-content-center col-12 col-md-4 mb-1 p-0" ] //<!--Card big taille-->

  armorStatic: Armor[]=[];
  weaponStatic: Weapon[]= [];
  personage: StaticPersonage[]= [];


  hero: CardComponent[]= [];
  armor: CardComponent[]= [];
  weapon: CardComponent[]= [];
  constructor(private mainService: MainService) { }

  ngOnInit(): void {
      this.mainService.getHero().subscribe((hero)=> {this.personage= hero;
      });
      this.mainService.getArmor().subscribe((armor)=>{this.armorStatic =armor;});
     this.mainService.getWeapon().subscribe((weapon)=>{this.weaponStatic =weapon;});
  }
  
  
  
}
