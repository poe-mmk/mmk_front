import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Event } from '@angular/router';
import { Armor } from 'src/app/interface/armor';
import { StaticPersonage } from 'src/app/interface/staticPersonage';
import { Weapon } from 'src/app/interface/weapon';
import { SearchService } from '../../services/search.service';
import { Options, LabelType } from "@angular-slider/ngx-slider";
import { CardComponent } from 'src/app/card/card.component';
import { Highligth } from 'src/app/interface/highligth';
import * as bootstrap from 'bootstrap';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  hero: StaticPersonage[] = [];
  armor: Armor[]=[];
  weapon: Weapon[]=[];
  format: string ="col-12 col-sm-6 col-lg-4 col-xl-3 my-2";
  tabFiltred: Highligth[]= [];
 categorie: boolean[] =[false, false, false];
  min: number = 0;
  max: number= 30000;
  // options du ng-slider 
  options: Options = {
    floor: 0,
    ceil: 30000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          this.min= value;
          return "<b>Min price:</b> $" + value;
        case LabelType.High:
          this.max= value;
          return "<b>Max price:</b> $" + value;
        default:
          return "$" + value;
      }
    }
  };
  asc?: boolean | undefined= undefined;
  ascPrice?: boolean | undefined= undefined;
  desc?: boolean | undefined= undefined;
  descPrice?: boolean | undefined= undefined;
  name: string ="";
  drop: bootstrap.Dropdown | undefined= undefined;
  constructor(private searchService: SearchService, private route: ActivatedRoute) { }

  ngOnInit(): void {


    let type: string =this.route.snapshot.params['type'];
    
    if(type){
      // BIzarrie ici ca check pas la bonne checkBox Je comprends pas pk 
      if(type==="hero"){
        (document.getElementById("Hero") as HTMLInputElement | null )!.checked = true;
        this.categorie[0]= true;
      }else if(type==="armor"){
        (document.getElementById("Armor") as HTMLInputElement | null )!.checked = true;
        this.categorie[1]= true;
      }else if(type==="weapon"){
        (document.getElementById("Weapon") as HTMLInputElement | null )!.checked = true;
        this.categorie[2]= true;
      }
    }

    this.searchService.getAllHero().subscribe((heros)=>{
      this.hero= heros;
    });
    this.searchService.getAllArmor().subscribe((armors)=>{
      this.armor= armors;
    });
    this.searchService.getAllWeapon().subscribe((weapons)=>{
      this.weapon= weapons;
    });
    setTimeout(() => {this.onChange();}, 500);
  }
  ngAfterViewInit(){
    this.drop =  new bootstrap.Dropdown(<Element>document.getElementById("drop"));
  }
  showDropdown(){
    this.drop!.toggle();
  }
  //#region  méthodes de comparaisons pour la méthode sort du tab a afficher
  compareByName(a1: any,a2: any){
    return a1.name.localeCompare(a2.name);
  }
  compareByPrice(a1: any, a2:any){
    return a1.price-a2.price;
  }
  //#endregion
  
  //setUp le filtre du name soit a true ou false en function de quelle button appel la méthode puis change le tab

  nameTri(e: any){
    this.asc = e.target.value;
    this.ascPrice= undefined;
    this.desc = undefined;
    this.descPrice = undefined;
    this.sortTri();
  }
  //setUp le filtre du prix soit a true ou false en function de quelle button appel la méthode puis change le tab
  priceTri(e:any){
    this.ascPrice = e.target.value;
    this.asc= undefined;
    this.desc = undefined;
    this.descPrice = undefined;
    this.sortTri();
  }
  nameTriDesc(e: any){
    this.asc = undefined;
    this.ascPrice= undefined;
    this.desc =  e.target.value;
    this.descPrice = undefined;
    this.sortTri();
  }
  priceTriDesc(e:any){
    this.ascPrice =undefined;
    this.asc= undefined;
    this.desc = undefined;
    this.descPrice =  e.target.value;
    this.sortTri();
  }
  // Méthode qui fill le tab d'highlight avec les trois autres en fonction des paramètres
  fillTab(){
    this.tabFiltred =[];
    let indice=0;
    let regExp =new RegExp(this.name ,'ig');;
    
    if(this.categorie[0] || ((!this.categorie[0]) &&(!this.categorie[1]) && (!this.categorie[2]))){
      for(let i =0 ; i< this.hero.length; i++){
        const h = this.hero[i];
        if(h.name.match(regExp)){
          if(h.price > this.min && h.price< this.max){
            this.tabFiltred[indice++] = {
              id:h.id,
              name: h.name,
              description: h.description,
              iconurl: h.iconURL,
              price: h.price,
              type: "personage"
            }
          }
        }
      }
    }
    if(this.categorie[1] || ((!this.categorie[0]) &&(!this.categorie[1]) && (!this.categorie[2]))){
      for(let j =0 ; j< this.armor.length; j++){
        const a = this.armor[j];
        if(a.name.match(regExp)){
          if(a.price! > this.min && a.price!< this.max){
            this.tabFiltred[indice++] = {
              id:a.id,
              name: a.name,
              description: a.description,
              iconurl: a.iconURL,
              price: a.price!,
              type: "armor"
            }
          }
        }
      }
    }
    if(this.categorie[2] || ((!this.categorie[0]) &&(!this.categorie[1]) && (!this.categorie[2]))){
      for(let k =0 ; k< this.weapon.length; k++){
        const w = this.weapon[k];
        if(w.name.match(regExp)){
          if(w.price! > this.min && w.price!< this.max){
            this.tabFiltred[indice++] = {
              id:w.id,
              name: w.name,
              description: w.description,
              iconurl: w.iconURL,
              price: w.price!,
              type: "weapon"
            }
          }
        }
      }
    }
  }

  // Change le Tab a afficher + applique le paramètre de tri
  onChange(){
    this.fillTab();
    this.sortTri();
  }
  // Tri le tab d'highlight en fonction du pararametrage de tri 
  sortTri(){ 
    if(this.asc)
      this.tabFiltred.sort(this.compareByName);
    if(this.ascPrice)
      this.tabFiltred.sort(this.compareByPrice);
    if(this.desc){
      this.tabFiltred.sort(this.compareByName);
      this.tabFiltred.reverse();
    }
    if(this.descPrice){
      this.tabFiltred.sort(this.compareByPrice);
      this.tabFiltred.reverse();
    }
    
  }
  //change la valeur des catégories a afficher 
  onCheckboxAll(e: any){
    if(e.target.checked){
      this.categorie[e.target.value]= true;
    }else{
      this.categorie[e.target.value]= false;
    }
    this.onChange();
  }
  // méthodes qui setup le filtreName et rapelle la méthode de remplissage du tab
  filterByName(e:any){
      this.name = e.target.value;
      this.onChange();
    }
  // reset les settings de tris
  onReset(){
    if(this.categorie[0]){
      this.categorie[0]= false;
      (document.getElementById("Hero") as HTMLInputElement |null )!.checked = false;
    }
    if(this.categorie[1]){
      this.categorie[1]= false;
      (document.getElementById("Armor") as HTMLInputElement |null )!.checked = false;
    }
    if(this.categorie[2]){
      this.categorie[2]= false;
      (document.getElementById("Weapon") as HTMLInputElement |null )!.checked = false;
    }
    this.asc= undefined;
    this.ascPrice= undefined;
    this.descPrice= undefined;
    this.desc= undefined;
    this.name= "";
    this.min= 0;
    this.max=30000;
    this.onChange();
  }
  // Methode qui est appeler quand on click sur ApplyPrice qui va appliquer le filtre (gestion de min et max dans options du slider)
  onClikPrice(){
    this.onChange();
  }
}
