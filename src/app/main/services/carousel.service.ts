import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarouselService {

    constructor(private httpClient: HttpClient){    }

    getHighLigth(): Observable<object[]>{
        return this.httpClient.get<object[]>("http://localhost:8080/api/highlightitem");
    }
}2