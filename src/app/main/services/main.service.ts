import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { StaticPersonage } from 'src/app/interface/staticPersonage';
import { Observable } from 'rxjs';
import { Weapon } from 'src/app/interface/weapon';
import { Armor } from 'src/app/interface/armor';

@Injectable({
  providedIn: 'root'
})
export class MainService {

    constructor(private httpClient: HttpClient) { }

    getHero(): Observable<StaticPersonage[]>{
        return this.httpClient.get<StaticPersonage[]>("http://localhost:8080/api/hero/highlight");
    }
    getWeapon(): Observable<Weapon[]>{
        return this.httpClient.get<Weapon[]>("http://localhost:8080/api/weapon/highlight");
    }
    getArmor(): Observable<Armor[]>{
        return this.httpClient.get<Armor[]>("http://localhost:8080/api/armor/highlight");
    }
}