import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {AuthStatus} from "../interface/auth-status";
import {LoginService} from "../utils/services/login.service";
import * as bootstrap from 'bootstrap';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    sub : Subscription = new Subscription()

    currentAuth? : AuthStatus;
    drop : bootstrap.Dropdown | undefined = undefined;
  constructor(private loginServices : LoginService) { }

    ngOnInit(): void {

        this.sub = this.loginServices.utilisateurLog$.subscribe((user) =>{
            this.currentAuth = user;
        });
    }
    ngAfterViewInit(){
      this.drop =new  bootstrap.Dropdown(<Element>document.getElementById("navDrop"));
    }
    showDrop(){
      this.drop?.toggle();
    }
}
