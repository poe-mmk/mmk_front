import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "./product.service";
import {Armor} from "../interface/armor";
import {Weapon} from "../interface/weapon";
import {StaticPersonage} from "../interface/staticPersonage";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productId!: number;
  productType!: 'personage' | 'armor' | 'weapon';
  product!: StaticPersonage | Armor | Weapon;

  constructor(private roote: ActivatedRoute, private productService: ProductService) { }

  ngOnInit(): void {
    this.productId = this.roote.snapshot.params["id"];
    this.productType = this.roote.snapshot.params["type"];
    this.productService.getProduct(this.productId, this.productType).subscribe(r => {
      this.product = r;
    });
  }

  addToCard(id: number, type: 'personage' | 'armor' | 'weapon'): void {
    this.productService.addToCard(id, type, this.product.name, this.product.iconURL);
  }

  isPersonage(o: any): o is StaticPersonage {
    return "strength" in o;
  }

  isArmor(o: any): o is Armor {
    return "defence" in o;
  }

  isWeapon(o: any): o is Weapon {
    return "damage" in o;
  }

}
