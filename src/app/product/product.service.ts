import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpResponse} from "@angular/common/http";
import {Observable, Subscription, tap} from "rxjs";
import {Armor} from "../interface/armor";
import {Weapon} from "../interface/weapon";
import {StaticPersonage} from "../interface/staticPersonage";
import { StatusService } from '../utils/services/status.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient, private status: StatusService) { }

  getProduct(id: number, type: 'personage' | 'weapon' | 'armor'): Observable<StaticPersonage | Armor | Weapon> {
    switch(type) {
      case 'personage' :
        return this.getPersonage(id);
      case 'weapon' :
        return this.getWeapon(id);
      case 'armor' :
        return this.getArmor(id);
    }
  }

  getPersonage(id: number): Observable<StaticPersonage> {
    return this.httpClient.get<StaticPersonage>("http://localhost:8080/api/hero/get/"+id);
  }

  getWeapon(id: number): Observable<Weapon> {
    return this.httpClient.get<Weapon>("http://localhost:8080/api/weapon/get/"+id);
  }

  getArmor(id: number): Observable<Armor> {
    return this.httpClient.get<Armor>("http://localhost:8080/api/armor/get/"+id);
  }

  addToCard(id: number, type: 'personage' | 'weapon' | 'armor', name: string, png: string){
    let typeInt: number = 0;
    switch (type) {
      case 'personage' :
        typeInt = 0;
        break ;
      case 'armor' :
        typeInt = 1;
        break ;
      case 'weapon' :
        typeInt = 2;
        break ;
    }
    this.status.envoyerStatus({
      response : "Ajout effectué",
      type : type,
      value: png,
      name: name
    });
    this.httpClient.put<any>("http://localhost:8080/api/cart/addarticle/"+typeInt+"/"+id, {}).subscribe();
    
  }
}
