import { Component, OnInit } from '@angular/core';
import {ConnectionDto} from "../../interface/connection-dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../utils/services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    connectionDto = new FormGroup({
        username : new FormControl('',[Validators.required,Validators.email]),
        password : new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)])
    })

    constructor(private loginService : LoginService) { }

    ngOnInit(): void {
    }

    onSubmit(){
        this.loginService.connection(this.connectionDto.value as ConnectionDto).subscribe(_ =>{
            this.loginService.getUser().subscribe((user) => {
                this.loginService.setAuthStatus({
                    connected : true,
                    user : user
                })
            })
        })
    }
}
