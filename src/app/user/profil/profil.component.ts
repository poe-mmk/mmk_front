import { Component, OnInit } from '@angular/core';
import {ProfilService} from "../../utils/services/profil.service";
import {User} from "../../interface/user";
import {Personage} from "../../interface/personage";
import {Save} from "../../interface/save";
import {LoginService} from "../../utils/services/login.service";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

    profil : User = {
        id : 0,
        lastName : "",
        firstName : "",
        pseudo : "",
        email : "",
        blackgold : 0,
        saves : []
    }

  constructor(private profilService: ProfilService, private loginService: LoginService) { }

  ngOnInit(): void {
    this.loginService.utilisateurLog$.subscribe(auth => {
        if (auth.connected)
            this.profil = auth.user!;
    })
      this.loginService.getUser().subscribe(u => {
          this.loginService.setAuthStatus({
              connected: ("email" in u),
              user: u
          })
      })
    this.profilService.getSave().subscribe(s =>{
        this.profil.saves = s ;
    })
  }

  getHero(save: Save): Personage | null {
        for (let personage of save.boards[save.currentBoard].personages)
            if (personage.iaId == null)
                return personage;
        return null;
  }

}
