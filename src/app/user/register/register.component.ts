import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ConnectionService} from "../../utils/services/connection.service";
import {InscriptionDto} from "../../interface/inscription-dto";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    inscriptionDto = new FormGroup({
        firstName: new FormControl('',[Validators.required,Validators.minLength(3)]),
        lastName: new FormControl('',[Validators.required,Validators.minLength(3)]),
        pseudo : new FormControl('',[Validators.required,Validators.minLength(3)]),
        email : new FormControl('',[Validators.required,Validators.email]),
        password : new FormControl('',[Validators.required,Validators.minLength(3),Validators.maxLength(20)])
    })

  constructor(private connectionService : ConnectionService) { }

  ngOnInit(): void {
  }

  onSubmit(){
      this.connectionService.inscription(this.inscriptionDto.value as InscriptionDto).subscribe();
  }

}
