import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ConnectionService} from "../../services/connection.service";
import {ConnectionDto} from "../../../interface/connection-dto";

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {

    connectionForm = new FormGroup({
        username: new FormControl(''),
        password: new FormControl('')
    });

    inscriptionForm = new FormGroup({
        firstName: new FormControl(''),
        lastName: new FormControl(''),
        pseudo: new FormControl(''),
        password: new FormControl(''),
        email: new FormControl('')
    })

    constructor(private connectionService: ConnectionService) { }

    ngOnInit(): void {
    }

    connectionMethod(): void {
        this.connectionService
            .connection(this.connectionForm.value as ConnectionDto)
            .subscribe();
    }

    inscriptionMethod(): void {

    }

}
