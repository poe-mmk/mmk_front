import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor, HttpResponse
} from '@angular/common/http';
import {Observable, tap} from 'rxjs';
import {Router} from "@angular/router";

@Injectable()
export class RedirectInterceptor implements HttpInterceptor {

  constructor(private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const cloneReq = request.clone(
        {
          withCredentials: true
        }
    )

    return next.handle(cloneReq)
        .pipe(
            tap((r: HttpEvent<any>) => {
                let data = r as HttpResponse<any>;
                if (data.body && "status" in data.body && "message" in data.body)
                    if (data.body.status === 302)
                        this.router.navigateByUrl(data.body.message);
                return "ok";
            })
        );
  }
}
