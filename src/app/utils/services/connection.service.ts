import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConnectionDto} from "../../interface/connection-dto";
import {Observable} from "rxjs";
import {InscriptionDto} from "../../interface/inscription-dto";

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

    constructor(private httpClient: HttpClient) { }

    connection(dto: ConnectionDto): Observable<string> {
        var form_data = new FormData();

        form_data.append('username', dto.username);
        form_data.append('password', dto.password);

        return this.httpClient.post<string>("http://localhost:8080/login", form_data);
    }

    inscription(inscriptionDto : InscriptionDto) : Observable<string>{
        return this.httpClient.post<string>('http://localhost:8080/api/user/inscription',inscriptionDto);
    }
}
