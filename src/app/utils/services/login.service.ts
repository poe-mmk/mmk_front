import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConnectionDto} from "../../interface/connection-dto";
import {BehaviorSubject, Observable} from "rxjs";
import {AuthStatus} from "../../interface/auth-status";
import {User} from "../../interface/user";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
    private currentUser =new BehaviorSubject<AuthStatus>({
        connected : false
    });

    utilisateurLog$ = this.currentUser.asObservable();

    constructor(private httpClient: HttpClient) {
        // On fait une requête vers l'API pour vérifier s'il y a eu des changements
        if (localStorage.getItem("user") != null) {
            this.setAuthStatus({
                connected: true,
                user: JSON.parse(localStorage.getItem("user")!)
            })
        }
        else {
            this.setAuthStatus({
                connected: false
            })
        }
    }


    connection(dto: ConnectionDto): Observable<string> {
        var form_data = new FormData();

        form_data.append('username', dto.username);
        form_data.append('password', dto.password);

        return this.httpClient.post<string>("http://localhost:8080/login", form_data);
    }

    getUser(): Observable<User>{
        return this.httpClient.get<User>('http://localhost:8080/api/user/get');
    }

    setAuthStatus(auth : AuthStatus){
        this.currentUser.next(auth);
        if (auth.user && "email" in auth.user) {
            localStorage.setItem("connected", "true");
            localStorage.setItem("user", JSON.stringify(auth.user));
        } else
            localStorage.setItem("connected", "false");
    }
}
