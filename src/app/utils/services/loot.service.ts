import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseMessage } from 'src/app/interface/response-message';

@Injectable({
  providedIn: 'root'
})
export class LootService {

  constructor(private httpClient: HttpClient) { }

  acceptArmor(): Observable<ResponseMessage>{
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/loot/acceptedArmor",  {});
  }
  acceptWeapon(): Observable<ResponseMessage>{
    return this.httpClient.post<ResponseMessage>("http://localhost:8080/api/loot/acceptedWeapon",  {});
  }

  
  refusedLoot(): Observable<ResponseMessage>{
    return this.httpClient.get<ResponseMessage>("http://localhost:8080/api/loot/refused", {})};
}
