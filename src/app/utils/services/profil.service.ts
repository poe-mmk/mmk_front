import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Save} from "../../interface/save";

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(private http : HttpClient) { }

  getSave(): Observable<Save[]>{
      return this.http.get<Save[]>("http://localhost:8080/api/user/getsave");
  }
}
