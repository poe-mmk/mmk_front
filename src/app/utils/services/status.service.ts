import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { StatusLoot } from 'src/app/interface/statusLoot';
import { StatusModal } from '../../interface/statusModal';
import {Status} from "../../interface/status";

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  private notification = new Subject<StatusModal>();
  notificationEnvoye$ = this.notification.asObservable();

  private papmnotif = new Subject<Status>();
  papmnotif$ = this.papmnotif.asObservable();
  private notificationLoot = new Subject<StatusLoot>();
  notificationEnvoyeLoot$ = this.notificationLoot.asObservable();

  constructor() { }

  envoyerStatus(status : StatusModal){
    this.notification.next(status);
  }
  envoyerStatusLoot(status: StatusLoot){
    this.notificationLoot.next(status);
  }
  notifPaPm(status: Status) {
    this.papmnotif.next(status);
  }
}
